make: gcc

debug : eatingsnake.c eatingsnake.h
	gcc -Wall -g -o eatingsnake eatingsnake.c eatingsnake.h -lncurses -lpthread

gcc : eatingsnake.c eatingsnake.h
	gcc -Wall -o eatingsnake eatingsnake.c eatingsnake.h -lncurses -lpthread

clean : 
	rm eatingsnake
	rm core
