#include"eatingsnake.h"

int main(int argc,char * argv[]){ 

	ncurses_init();

	for(;;){
		//error handler
		ERROR_TYPE error_code = setjmp(stack_buf);
		if(error_code!=ERROR_NOERROR){
			clear();
			move(0,0);
			switch(error_code){
			case ERROR_FAILED_PTHREAD_CREATE:
				addstr("cant create another thread!\n");
				break;
			case ERROR_FAILED_MALLOC:
				addstr("cant malloc!\n");
				break;
			case ERROR_FAILED_SCANF:
				addstr("cant scanf properly!"
					"The rank.dat file maybe not standard!\n");
				break;
			case ERROR_UNDEFINED_DIRECTION:
				addstr("there must be something wrong with the code "
					"in the function snake_move()");
				break;
			case ERROR_FAILED_FILE_OPEN:
				addstr("the file rank.dat might be missing"
					" and have some problem to open");
				break;
			case ERROR_FAILED_FILE_READ:
				addstr("illegal content format in the rank.dat");
				break;
			case ERROR_FAILED_FILE_SAVE:
				addstr("cant save the file!");
				break;
			case ERROR_QUEUE_OVERFLOW:
				addstr("queue overflow! contact the stupid developer!");
				break;
			case ERROR_NOERROR:
				addstr("the elf must be changed! Stop using it!");
				break;
			}
			refresh();
			getch();
			clear();
			return error_code;
		}


		//welcome those who can come up with a better choice 
		//rather than use goto 
input:

		//mainpart
		menu_output();
		
		MENU_CHOICE menu_choice = getch()-'0';
		
		//yes, i dont wanna use switch
		switch(menu_choice){
		case MENU_CHOICE_PLAY:
			menu_choice_play();
			break;
		case MENU_CHOICE_LIST:
			menu_choice_list();
			break;
		case MENU_CHOICE_HELP:
			menu_choice_help();
			break;
		case MENU_CHOICE_QUIT:
			menu_choice_quit();
			break;
		default:
			clear();
			goto input;
		}
		clear();
	}
}

	void
ncurses_init		(void)
{
	initscr();
	noecho();
	curs_set(0);
	//cbreak();
}

//was placed in the function menu_choice_quit()
	void
ncurses_end		(void)
{
	endwin();
}

	void
menu_output		(void)
{
	addstr("The Eating Snake\n"
		"1.\tPlay\n"
		"2.\tRank\n"
		"3.\tHelp\n"
		"4.\tQuit\n");
}	

	void
menu_choice_play	(void)
{
	game_init();
	game_start();
	game_end();
}

	void
menu_choice_list	(void)
{
	clear();
	mvprintw(0,((COLS-strlen(rank_welcome_title))/2),rank_welcome_title);

	rank_list_init();
	rank_list_get();

	for(int i=0;i<NUM_INDIVIDUAL;++i)
		mvprintw(((LINES-NUM_INDIVIDUAL)>>1)+i,(COLS-8)>>1,"%d:%d\t%s\n",i,rank_list[i]->grade,rank_list[i]->name);

	rank_list_release();

	refresh();
	getch();
	clear();
}


	void
menu_choice_help	(void)
{
	clear();
	addstr("This is traditional eaitng snake game\n"
		"and you use lower case of wasd to"
		" control the direction of snake\n"
		"and you use p to pause the game\n"
		"you can use q to quit game when"
		"you dont wanna play while you are playing\n"
		"and use c to continue\n");
	refresh();
	getch();
	clear();
}

	void
menu_choice_quit	(void)
{
	ncurses_end();
	exit(0);
}

	void
queue_init		(struct Queue *queue, size_t element_size,int length){
	queue->head=0;
	queue->tail=0;
	queue->max_length=length;
	queue->element_size=element_size;
	queue->data=(void*)malloc((queue->max_length)*(queue->element_size));
}

	void
queue_pop_front		(struct Queue *queue){
	//beacause the push_back has detect for overflow,we dont detect it again
	queue->head=((queue->head) + (queue->max_length)+1) % (queue->max_length);
}

	void
queue_push_back		(struct Queue *queue, void *value){
	/*
	if(queue->head==queue->tail)
		longjmp(stack_buf,ERROR_QUEUE_OVERFLOW);
	//the judgement is fairly wrong
	*/
	memcpy(
			queue->data + (queue->tail)*(queue->element_size),
			value,
			queue->element_size
	);
	queue->tail = (queue->tail + 1) % (queue->max_length);
}


	void
game_init		(void)
{
	//threadID_key_process_init
	{
		int err = pthread_create(&threadID_key_process,NULL,key_process,NULL);
		if(err){
#ifdef _DEBUG
		       	printf("%s\n",strerror(err));
#endif
			longjmp(stack_buf,ERROR_FAILED_PTHREAD_CREATE);
		}
	}

	//mem_block_raw_init()
	{
		//we dont map the boundrary
		mem_block_raw=(char*)malloc(((LINES-2)*sizeof(char*)+(LINES-2)*(COLS-2)*sizeof(char)));
		if(!mem_block_raw)
			longjmp(stack_buf,ERROR_FAILED_MALLOC);
	}

	//mem_block_temp_init()
	{
		mem_block_temp = mem_block_raw;
	}

	//game_map_init();
	{
		//the game_map seems to be unused XD
		game_map=(char**)malloc((LINES-2)*sizeof(char*));
		if(!game_map)
			longjmp(stack_buf,ERROR_FAILED_MALLOC);
		for(int i=0;i<LINES-2;++i){
			game_map[i]=mem_block_temp;
			mem_block_temp+=(COLS-2)*sizeof(char);
		}
	}

	//snake_init();
	{
		snake.snake_length		=SNAKE_DEFAULT_LENGTH;
		snake.snake_length_ungrow	=SNAKE_DEFAULT_LENGTH_UNGROW;
		snake.snake_speed		=SNAKE_DEFAULT_SPEED;
		snake.snake_direction		=SNAKE_DEFAULT_DIRECTION;
		//we have boundrary,so we have to decrease 2
		queue_init(&snake.body,sizeof(struct Vec2),(COLS-2)*(LINES-2));
		struct Vec2 snake_section={LINES>>1,COLS>>1};
		//the snake tail
		queue_push_back(&snake.body,&snake_section);
		//the snake head
		--snake_section.y;
		queue_push_back(&snake.body,&snake_section);
	}

	//apple_init();
	apple_produce();

	//communication_init();
	communication_input=0;

	//random_seed_init();
	random_seed=0xffffffff&time(NULL);

}

	void
game_end		(void)
{
	//let thread stop
	communication_input=0xFF;
	pthread_join(threadID_key_process,NULL);

	rank_list_init();
	rank_list_get();
	game_over_info_output();
	getch();
	if(rank_list[NUM_INDIVIDUAL-1]->grade<snake.snake_length+snake.snake_length_ungrow){
		game_over_grade_record();
		game_over_grade_output();
	}

	//The memblockraw here is the game_map and its first dimension pointers are unfree
	//and gamemap's first and second dimension is freed by the way
	free(mem_block_raw);

	//avoid unnecessary chaos
	mem_block_raw	=NULL;
	mem_block_temp	=NULL;
	game_map	=NULL;

	memset(&snake,0,sizeof(struct Snake));

	threadID_key_process=0;

	communication_input=0;
}

	void
game_start		(void)
{
	interface_init();
	for(;;){
		if(communicate()==-1)
			goto game_over;
		snake_move();
		if(hit_judge()==1)
			goto game_over;
		interface_refresh();
		usleep((500-(snake.snake_length<<2))<<8);
	}
game_over:
	return;
}

// return 0 means not hit and 1 means hit
	int	
hit_judge		(void)
{
	//hit judge,must after tail move 
	//or the situation of snake form a ring will cause hit
	struct Vec2 *head=(struct Vec2 *)(snake.body.data+(snake.body.tail-1)*snake.body.element_size);

	if(head->x<=0||head->y<=0||head->x>=COLS-1||head->y>=LINES-1)
		goto game_over;
	for(
		struct Vec2 *tmp=(struct Vec2 *)(snake.body.data+snake.body.head*snake.body.element_size);
		tmp!=head;
		tmp=((tmp+1)>(struct Vec2 *)(snake.body.data+snake.body.max_length*snake.body.element_size))?(struct Vec2*)snake.body.data:(tmp+1)
	   )
		if((tmp->x==head->x)&&(tmp->y==head->y))
			goto game_over;
	return 0;
game_over:
	return 1;
}

	void*
key_process		(void *emm)
{
	volatile char temp;
	for(;;){
		temp=getch();
		if(communication_input == 0xFF)
			break;
		communication_input=temp;
	}
	pthread_exit(0);
}

	int
communicate		(void)
{
	if(communication_input!=snake.snake_direction){
		switch((int)communication_input){
		case GAME_INSTRUCTION_DIRECTION_UP:
			if(snake.snake_direction!=DIRECTION_DOWN)
				snake.snake_direction=communication_input;
			break;	
		case GAME_INSTRUCTION_DIRECTION_DOWN:
			if(snake.snake_direction!=DIRECTION_UP)
				snake.snake_direction=communication_input;
			break;	
		case GAME_INSTRUCTION_DIRECTION_LEFT:
			if(snake.snake_direction!=DIRECTION_RIGHT)
				snake.snake_direction=communication_input;
			break;	
		case GAME_INSTRUCTION_DIRECTION_RIGHT:
			if(snake.snake_direction!=DIRECTION_LEFT)
				snake.snake_direction=communication_input;
			break;	

		case GAME_INSTRUCTION_PAUSE:
			for(;;){
				if(communication_input==GAME_INSTRUCTION_CONTINUE)
					break;
				sleep(1);
			}
			break;
		case GAME_INSTRUCTION_CONTINUE:
			//because the game is not pause, we do nothing
			break;
		case GAME_INSTRUCTION_QUIT:
			goto game_over;

		default:
			break;
		}
	}
	return 0;
	
game_over:
	return -1;
}

	void
apple_produce		(void)
{
	for(;;){
		random_seed=214013*random_seed+2531011;
		apple.apple_grade=random_seed%9+1;
		apple.apple_position.y=random_seed%(LINES-2)+1;
		apple.apple_position.x=random_seed%(COLS-2)+1;
		for(int i=snake.body.head;i<snake.body.tail;i=(i+1)%snake.body.max_length)
			if(
				ABS(apple.apple_position.x-((struct Vec2*)(snake.body.data+i*snake.body.element_size))->x)<2
				&&
				ABS(apple.apple_position.y-((struct Vec2*)(snake.body.data+i*snake.body.element_size))->y)<2
			)
				continue;
		break;
	}
	//for debug
	return;
}

	void
apple_eaten		(void)
{
	snake.snake_length_ungrow+=apple.apple_grade;
	apple_produce();
}

	void
interface_init		(void)
{
	clear();
	for(int i=0;i<COLS;++i){
		mvaddch(0,i,STRUCT_MATERIAL_WALL);
		mvaddch(LINES-1,i,STRUCT_MATERIAL_WALL);
	}	
	for(int i=1;i<LINES-1;++i){
		mvaddch(i,0,STRUCT_MATERIAL_WALL);
		mvaddch(i,COLS-1,STRUCT_MATERIAL_WALL);
	}

	{
		struct Vec2 *i=snake.body.data+snake.body.head*snake.body.element_size;
		mvaddch(i->y,i->x,STRUCT_MATERIAL_SNAKE_TAIL);
		for(
			i++; 
			i<=(struct Vec2 *)(snake.body.data+(snake.body.tail-2)*snake.body.element_size);
			i++
		)
			mvaddch(i->y,i->x,STRUCT_MATERIAL_SNAKE_BODY);
		mvaddch(i->y,i->x,STRUCT_MATERIAL_SNAKE_HEAD);
	}

	//show apple
	mvaddch(apple.apple_position.y,apple.apple_position.x,apple.apple_grade+'0');

	refresh();
}

	void
interface_refresh	(void)
{
	//actually the head of the data is the tail of the snake
	struct Vec2 *tail_last	=snake.body.data+((snake.body.head+snake.body.max_length-1) % snake.body.max_length) * snake.body.element_size;
	struct Vec2 *head_last	=snake.body.data+((snake.body.tail+snake.body.max_length-2) % snake.body.max_length) * snake.body.element_size;
	struct Vec2 *head	=snake.body.data+((snake.body.tail+snake.body.max_length-1) % snake.body.max_length) * snake.body.element_size;
	struct Vec2 *tail	=snake.body.data+(snake.body.head)*snake.body.element_size;
	//we should clear the tail_last
	mvaddch(tail_last->y	,tail_last->x,	STRUCT_MATERIAL_EMPTY);
	mvaddch(head_last->y	,head_last->x,	STRUCT_MATERIAL_SNAKE_BODY);
	//we should care about when the snake's length is 2 the head_last is tail
	//so we use this order
	mvaddch(tail->y		,tail->x,	STRUCT_MATERIAL_SNAKE_TAIL);

	mvaddch(head->y		,head->x,	STRUCT_MATERIAL_SNAKE_HEAD);

	//because the poistion is relative to the upper left corner of the map rether than the interface
	mvaddch(apple.apple_position.y,apple.apple_position.x,apple.apple_grade+'0');

	refresh();
}

//1. head move
//2. apple judge
//3. tail move (if the ungrow length==0)
//4, hit judge

//WARNING:the problem is if the first move the snake eat the apple 
//the tail_last will be undefined

//I solved by not let the apple produced around the head

	int
snake_move		(void)
{
	//head move
	struct Vec2 head = *(struct Vec2*)(snake.body.data+(snake.body.tail-1)*snake.body.element_size);
	switch(snake.snake_direction)
	{
	case DIRECTION_UP:
		head.y--;
		break;
	case DIRECTION_DOWN:
		head.y++;
		break;
	case DIRECTION_LEFT:
		head.x--;
		break;
	case DIRECTION_RIGHT:
		head.x++;
		break;
	default:
		longjmp(stack_buf,ERROR_UNDEFINED_DIRECTION);
		break;
	}
	queue_push_back(&(snake.body),&head);

	//apple judge
	if(
		head.x==apple.apple_position.x
		&&
		head.y==apple.apple_position.y
	  )
		apple_eaten();

	//tail move
	if(snake.snake_length_ungrow==0)
		queue_pop_front(&(snake.body));
	else{
		++snake.snake_length;
		--snake.snake_length_ungrow;
	}

	return 0;
}
	
	void
rank_list_get		(void)
{
	FILE *file = fopen("rank.dat","r");
	if(file == NULL)
		longjmp(stack_buf,ERROR_FAILED_FILE_OPEN);
	clear();
	//because the name is the head of the array, so we use its addr
	for(int i=0;i<NUM_INDIVIDUAL;++i)
		if(!fscanf(file,"%d %s",&(rank_list[i]->grade),rank_list[i]->name)){
			fclose(file);
			longjmp(stack_buf,ERROR_FAILED_FILE_READ);
		}
	fclose(file);
}

	void
rank_list_init		(void)
{
	mem_block_raw=(char*)calloc(NUM_INDIVIDUAL,SIZEOF_INDIVIDUAL);
	mem_block_temp=mem_block_raw;
	for(int i=0;i<NUM_INDIVIDUAL;++i){
		rank_list[i]=(struct Individual*)mem_block_temp;
		mem_block_temp+=SIZEOF_INDIVIDUAL;
	}
}

	void
rank_list_release	(void)
{
	free(mem_block_raw);
	mem_block_temp=NULL;
	memset(rank_list,0,NUM_INDIVIDUAL*SIZEOF_INDIVIDUAL);
}


//only the individual con be inserted con goes into the function
	void
rank_list_insert	(struct Individual *key)
{
	int i;
	for(i=NUM_INDIVIDUAL-2;(i>=0)&&!(rank_list[i]->grade>=key->grade);--i)
		rank_list[i+1]=rank_list[i];
	rank_list[i+1]=key;
}

	void
game_over_info_output	(void)
{
	clear();
	if(LINES<NUM_INDIVIDUAL){
		mvaddstr(LINES>>1,0,"the space is too small");
		return;
	}

	for(int i=0;i<NUM_INDIVIDUAL;++i)
		mvprintw(((LINES-NUM_INDIVIDUAL)>>1)+i,(COLS-8)>>1,"%d:%d\t%s\n",i,rank_list[i]->grade,rank_list[i]->name);
	refresh();
}

	void
game_over_grade_record	(void)
{
	move(LINES-2,0);
	addstr("type in your name: ");
	struct Individual tmp;
	tmp.grade=snake.snake_length+snake.snake_length_ungrow;
	//to visuallize the chars input
	echo();
	scanw("%"TO_STRING(MAX_NAME_LENGTH)"s",tmp.name);
	noecho();
	rank_list_insert(&tmp);
	FILE *file = fopen("rank.dat.swp","w");
	for(int i=0;i<NUM_INDIVIDUAL;++i)
		if(!fprintf(file,"%d %s\n",rank_list[i]->grade,rank_list[i]->name)){
			fclose(file);
			longjmp(stack_buf,ERROR_FAILED_FILE_SAVE);
		}
	fclose(file);
	rename("rank.dat.swp","rank.dat");
}

	void
game_over_grade_output	(void)
{
	clear();
	mvprintw(0,0,"your grade is %d",snake.snake_length+snake.snake_length_ungrow);
	for(int i=0;i<NUM_INDIVIDUAL;++i)
		mvprintw(((LINES-NUM_INDIVIDUAL)>>1)+i,(COLS-8)>>1,"%d:%d %s\n",i,rank_list[i]->grade,rank_list[i]->name);
	refresh();
	getch();
}

