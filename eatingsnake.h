#ifndef EATINGSNAKE_H
#define EATINGSNAKE_H
//---------------------------these should be placed in head file
//

//I decide to place the struct like Snake's default value in the const struct

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ncurses.h>
//trying to create another thread to process keyboard input 
//i regreted after found the openmp have section
#include<pthread.h>	
#include<unistd.h>
#include<sys/time.h>
#include<setjmp.h>


/*
//the influence of Linus
#define	memcpy	memmove		
*/
#define NUM_INDIVIDUAL	10
#define MAX_NAME_LENGTH	20

#define TO_STRING(x)	#x
#define ABS(x)		((x)<0?(-x):(x))
#define SIZEOF_INDIVIDUAL (sizeof(int)+(MAX_NAME_LENGTH+1)*sizeof(char))

//----------------global types
typedef enum MENU_CHOICE{
	MENU_CHOICE_PLAY=1,
	MENU_CHOICE_LIST=2,
	MENU_CHOICE_HELP=3,
	MENU_CHOICE_QUIT=4
}MENU_CHOICE;

typedef enum GAME_INSTRUCTION{
	GAME_INSTRUCTION_PAUSE			='p',
	GAME_INSTRUCTION_QUIT			='q',
	GAME_INSTRUCTION_CONTINUE		='c',
	GAME_INSTRUCTION_DIRECTION_UP		='w',
	GAME_INSTRUCTION_DIRECTION_DOWN		='s',
	GAME_INSTRUCTION_DIRECTION_LEFT		='a',
	GAME_INSTRUCTION_DIRECTION_RIGHT	='d'
}GAME_INSTRUCTION;

typedef enum STRUCT_MATERIAL{
	STRUCT_MATERIAL_EMPTY			=' ',
	STRUCT_MATERIAL_WALL			='#',
	STRUCT_MATERIAL_APPLE_BASE		='0',
	STRUCT_MATERIAL_SNAKE_HEAD		='E',
	STRUCT_MATERIAL_SNAKE_BODY		='S',
	STRUCT_MATERIAL_SNAKE_TAIL		='O',
}STRUCT_MATERIAL;

typedef enum DIRECTION{
	DIRECTION_UP			='w',
	DIRECTION_DOWN			='s',
	DIRECTION_LEFT			='a',
	DIRECTION_RIGHT			='d',
}DIRECTION;

typedef enum SNAKE_DEFAULT{
	SNAKE_DEFAULT_LENGTH		=2,
	SNAKE_DEFAULT_LENGTH_UNGROW	=0,
	SNAKE_DEFAULT_SPEED		=10,
	SNAKE_DEFAULT_DIRECTION		=DIRECTION_UP,
	//there is no body 
}SNAKE_DEFAULT;

typedef enum ERROR_TYPE{
	ERROR_NOERROR				=-0X0,
	ERROR_UNDEFINED_DIRECTION		=-0X1,
	ERROR_FAILED_PTHREAD_CREATE		=-0X2,
	ERROR_FAILED_MALLOC			=-0X4,
	ERROR_FAILED_SCANF			=-0X8,
	ERROR_FAILED_FILE_SAVE			=-0X10,
	ERROR_FAILED_FILE_OPEN			=-0X20,
	ERROR_FAILED_FILE_READ			=-0X40,
	ERROR_QUEUE_OVERFLOW			=-0X80,
}ERROR_TYPE;

struct	Vec2{
	int	y;
	int	x;
};

struct	Apple{
	struct Vec2	apple_position;
	int		apple_grade;
};

struct	Individual{
	int	grade;
	char 	name[1];
};

struct	Queue{
	int 	head;
	int 	tail;
	int	max_length;
	size_t	element_size;
	void 	*data;
};

//snake's body tail and head is the actually head and tail
struct	Snake{
	int 		snake_length;
	int		snake_length_ungrow;
	int		snake_speed;
	DIRECTION	snake_direction;
	struct Queue	body;
};


//--------------end of global types


//-------------global var
unsigned		random_seed;

char			*mem_block_raw;

char			*mem_block_temp;

char			**game_map;

volatile unsigned char	communication_input;

const char 		rank_welcome_title[]="Rank List\n";

const char 		rank_file[]="rank.dat";

pthread_t		threadID_key_process;

jmp_buf			stack_buf;

struct	Snake		snake;

struct	Apple		apple;

struct	Individual	*rank_list[NUM_INDIVIDUAL];


//-------------end of global var


//------------ncurses operation

void	ncurses_init		(void);

void	ncurses_end		(void);


//-------------end of ncurses operation


//------------menu choice

void	menu_output			(void);

void	menu_choice_play		(void);

void	menu_choice_list		(void);

void	menu_choice_help		(void);

void	menu_choice_quit		(void);

//-----------end of menu choice


//-------Queue operation

void	queue_init			(struct Queue *queue, size_t element_size, int length);

void	queue_pop_front			(struct Queue *queue);

void	queue_push_back			(struct Queue *queue, void *value);

//-----------------end of queue operation



//-------------game opeation(big map)

void	game_init			(void);

void	game_end			(void);

void	game_start			(void);

//-------------end of game operation(big map)



//---------------function used by another thread

void*	key_process			(void*);

//----------------------------------------------



//-------------communication_input process

int	communicate			(void);

//---------------------------------------------




//-------------snake operation

int	snake_move			(void);

//-----------end of snake operation



//------------apple operation

void	apple_produce			(void);

void	apple_eaten			(void);

//-----------end of apple operation




//---------interface

//first		print interface 
//second 	print apple
//last		print snake
void	interface_init			(void);


//if apple eaten?
//if game_over?
// if there is some thing wrong we return -1 
//and the play loop game_start terminated
void	interface_refresh		(void);

//-----------end of interface

// hit judge

int	hit_judge			(void);

//------------end of hit judge


//-----------game over

//fulfill the rank_list array
void	game_over_info_output		(void);

//insert and write to the hard disk 
void	game_over_grade_record		(void);

void	game_over_grade_output		(void);

//------------end of game over




//-----------------rank list operation 

void	rank_list_init			(void);

void	rank_list_release		(void);

//the rank list should be placed in the "rank.dat"
void	rank_list_get			(void);

//should test if it can be inserted, if not dont insert
void	rank_list_insert		(struct Individual *key);

//----------------end of list operation



//----------------------------------end of head file

#endif
